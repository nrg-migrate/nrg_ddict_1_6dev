/*
 * org.nrg.ddict.services.DataDictionaryService
 *
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * XNAT is an open-source project of the Neuroinformatics Research Group.
 * Released under the Simplified BSD.
 *
 * Last modified 5/1/14 10:27 AM
 */

package org.nrg.ddict.services;

import org.nrg.ddict.entities.DictionaryResource;
import java.util.Collection;

/**
 * The data dictionary service interface is the primary means of accessing data dictionary instances
 * within the XNAT service context.
 */
public interface DataDictionaryService{

	public void addResources(Collection<DictionaryResource> resources);

	public void addResource(DictionaryResource resource) throws Exception;

	public DictionaryResource getResource(String uri) throws Exception;

	public Collection<DictionaryResource> getResources(Collection<String> uris);

	public void removeResource(String uri);

	public void removeResource(DictionaryResource resource);

	public void removeResources(Collection<String> uris);

	public int getNumberOfResources();

	public void clearDictionary();
}
