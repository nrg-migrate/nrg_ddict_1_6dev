/*
 * org.nrg.ddict.services.impl.hibernate
 *
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * XNAT is an open-source project of the Neuroinformatics Research Group.
 * Released under the Simplified BSD.
 *
 * Last modified 5/1/14 10:34 AM
 */

package org.nrg.ddict.services.impl.hibernate;

import org.nrg.ddict.entities.DictionaryResource;
import org.nrg.ddict.services.DataDictionaryService;
import org.springframework.stereotype.Service;
import java.util.Collection;

/**
 * Service for managing data dictionary resources.
 */
@Service
public class HibernateResourceService implements DataDictionaryService {

	@Override
	public void addResources(Collection<DictionaryResource> resources) {

	}

	@Override
	public void addResource(DictionaryResource resource) {

	}

	@Override
	public DictionaryResource getResource(String uri) {
		return null;
	}

	@Override
	public Collection<DictionaryResource> getResources(Collection<String> uris) {
		return null;
	}

	@Override
	public void removeResource(String uri) {

	}

	@Override
	public void removeResource(DictionaryResource resource) {

	}

	@Override
	public void removeResources(Collection<String> uris) {

	}

	@Override
	public int getNumberOfResources() {
		return 0;
	}

	@Override
	public void clearDictionary() {

	}
}
