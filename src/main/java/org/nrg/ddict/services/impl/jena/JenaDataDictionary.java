package org.nrg.ddict.services.impl.jena;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.tdb.TDBFactory;
import org.nrg.ddict.entities.DictionaryResource;
import org.nrg.ddict.entities.impl.DefaultDictionaryResource;
import org.nrg.ddict.entities.impl.DictionaryItem;
import org.nrg.ddict.entities.impl.DictionaryLiteral;
import org.nrg.ddict.services.DataDictionaryService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
public class JenaDataDictionary implements DataDictionaryService, InitializingBean {

    @Value("${tdb.dataset.dir}")
    private String _datasetDir;

	private Dataset _dataset;

    /**
     * Creates a dataset in the directory specified by the data set directory.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
		File dir = new File(_datasetDir);
		if(!dir.exists()){
			if (!dir.mkdir()) {
                throw new RuntimeException("Failed to create the specified directory: " + _datasetDir);
            }
		}
		_dataset = TDBFactory.createDataset(_datasetDir);
	}

	@Override
	public void clearDictionary(){
		_dataset.begin(ReadWrite.WRITE);
		_dataset.getDefaultModel().removeAll();
		_dataset.commit();
		_dataset.end();
	}

	/**
	 * Function will add a Resource to the Dictionary
	 * @param resource - The Resource we are adding
	 * @throws Exception
	 */
	@Override
	public void addResource(DictionaryResource resource) throws Exception{
		_dataset.begin(ReadWrite.WRITE);
		Model model = _dataset.getDefaultModel();

		try{
			addProperties(model.createResource(resource.getId()),resource.getProperties());
			_dataset.commit();
		}catch(Exception e){
			throw new Exception("Failed to add Resource.",e);
		}finally{
			_dataset.end();
		}
	}

	/**
	 * Function will add multiple Resources to the Dictionary
	 * @param resources - A Collection of Resources to add.
	 */
	@Override
	public void addResources(Collection<DictionaryResource> resources) {
		//TODO
	}

	/**
	 * Recursive function to add properties to a Resource
	 * @param resource - The resource we are adding properties to
	 * @param properties = A list of DictionaryItem Properties
	 * @return The indicated resource.
	 */
	private Resource addProperties(Resource resource, Map<String,DictionaryItem> properties){
		if(properties == null || properties.isEmpty()){
			return resource;
		}
		for(Map.Entry<String,DictionaryItem> entry : properties.entrySet()){
			DictionaryItem di = entry.getValue();
			if(di.isLiteral()){
				DictionaryLiteral dl = (DictionaryLiteral)di;
				resource.addProperty(resource.getModel().createProperty(entry.getKey()), dl.getValue());
			}else if(di.isResource()){
				Resource r = resource.getModel().createResource(entry.getKey());
				resource.addProperty(resource.getModel().createProperty(entry.getKey()),r);
				addProperties(r,((DictionaryResource) di).getProperties());
			}
		}
		return resource;
	}

	/**
	 * Function to retrieve a Resource from the dictionary
	 * @param uri - the URI of the Resource we are interested in
	 * @return Dictionary Resource
	 * @throws Exception
	 */
	@Override
	public DictionaryResource getResource(String uri) throws Exception{
		_dataset.begin(ReadWrite.READ);
		try {
			Model m = _dataset.getDefaultModel();
			Resource r = m.getResource(uri);
			return new DefaultDictionaryResource(uri,getProperties(r.listProperties()));
		} finally {
			_dataset.end();
		}
	}

	/**
	 * A recursive function to get the properties of a DictionaryResource
	 * @param it - A Jena StatementIterator that will iterate over the properties
	 * @return A Map of DictionaryItems properties
	 */
	private Map<String,DictionaryItem> getProperties(StmtIterator it){
		Map<String,DictionaryItem> properties = new HashMap<String,DictionaryItem>();
		while(it.hasNext()){
			Statement st = it.next();
			RDFNode n = st.getObject();
			if(n.isLiteral()){
				// If the node is a literal, create a new DictionaryLiteral
				properties.put(st.getPredicate().getURI(), new DictionaryLiteral(st.getLiteral().toString()));
			}else if(n.isResource()){
				// If the node is a resource, create a new DictionaryResource
				properties.put(st.getPredicate().getURI(), new DefaultDictionaryResource(n.asResource().getURI(),getProperties(n.asResource().listProperties())));
			}
		}
		return properties;
	}

	/**
	 * FUnction will retrieve multiple Resources from the Dictionary.
	 * @param uris - The URIS of the Resources we are interested in
	 * @return - A collection of DictionaryResources
	 */
	@Override
	public Collection<DictionaryResource> getResources(Collection<String> uris) {
		//TODO
		return null;
	}

	/**
	 * Function will remove a Resource from the dictionary
	 * @param uri - the URI of the Resource we wish to remove
	 */
	@Override
	public void removeResource(String uri){
		_dataset.begin(ReadWrite.WRITE);
		Model m = _dataset.getDefaultModel();

		m.remove(m.listStatements(m.getResource(uri),null,(RDFNode)null));

		_dataset.commit();
		_dataset.end();
	}

	/**
	 * Function will remove a Resource from the Dictionary
	 * @param resource - The Resource we wish to remove.
	 */
	@Override
	public void removeResource(DictionaryResource resource){
		removeResource(resource.getId());
	}

	/**
	 * Function will remove multiple Resources from the Dictionary.
	 * @param uris - A Collection of URIS to remove.
	 */
	@Override
	public void removeResources(Collection<String> uris){
		//TODO
	}

	/**
	 * Function counts the number of Resources in the Dictionary
	 * @return int - the number of Resources
	 */
	@Override
	public int getNumberOfResources(){
		_dataset.begin(ReadWrite.READ);
		Model m = _dataset.getDefaultModel();

		ResIterator it = m.listSubjects();
		int count = 0;
		while(it.hasNext()){
			count++;
			it.next();
		}
		_dataset.end();
		return count;
	}
}
