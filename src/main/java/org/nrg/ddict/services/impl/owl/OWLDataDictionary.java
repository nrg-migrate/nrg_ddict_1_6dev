package org.nrg.ddict.services.impl.owl;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.tdb.TDBFactory;
import org.nrg.ddict.entities.DictionaryResource;
import org.nrg.ddict.entities.impl.DictionaryItem;
import org.nrg.ddict.entities.impl.DictionaryLiteral;
import org.nrg.ddict.services.DataDictionaryService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.util.Collection;
import java.util.Map;

/**
 * Created by James on 5/14/14.
 */
public class OWLDataDictionary implements DataDictionaryService, InitializingBean {

    @Value("${tdb.dataset.dir}")
	private String _datasetDir;

	private Dataset _dataset;

	/**
	 * Creates a dataset in the directory specified by the data set directory.
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		File dir = new File(_datasetDir);
		if(!dir.exists()){
			if (!dir.mkdir()) {
				throw new RuntimeException("Failed to create the specified directory: " + _datasetDir);
			}
		}
		_dataset = TDBFactory.createDataset(_datasetDir);
	}

	private void createDataset(String directory){
		File dir = new File(directory);
		if(!dir.exists()){
			dir.mkdir();
		}
		_dataset = TDBFactory.createDataset(directory);
	}

	private OntModel getOntModel(){
		return ModelFactory.createOntologyModel(new OntModelSpec(OntModelSpec.OWL_MEM_TRANS_INF), _dataset.getDefaultModel());
	}

	@Override
	public void addResources(Collection<DictionaryResource> resources) {

	}

	@Override
	public void addResource(DictionaryResource resource) throws Exception{
		_dataset.begin(ReadWrite.WRITE);
		OntModel om = this.getOntModel();
		try{
			OntClass oc = om.createClass(resource.getId());
			addProperties(oc,resource.getProperties());
			_dataset.commit();
		}catch(Exception e){
			throw new Exception("Failed to add resource.",e);
		}finally {
			_dataset.end();
		}
	}

	private void addProperties(OntClass oc, Map<String,DictionaryItem> properties){
		for(Map.Entry<String,DictionaryItem> entry : properties.entrySet()){
			DictionaryItem di = entry.getValue();
			if(di.isLiteral()){
				DictionaryLiteral dl = (DictionaryLiteral)di;
			}else if(di.isResource()){
				DictionaryResource dr = (DictionaryResource)di;
			//	addProperties(oc.addSubClass(new OntClass(dr.getId())), dr.getProperties());
			}
		}
	}

	@Override
	public DictionaryResource getResource(String uri) {
		return null;
	}

	@Override
	public Collection<DictionaryResource> getResources(Collection<String> uris) {
		return null;
	}

	@Override
	public void removeResource(String uri) {

	}

	@Override
	public void removeResource(DictionaryResource resource) {

	}

	@Override
	public void removeResources(Collection<String> uris) {

	}

	@Override
	public int getNumberOfResources() {
		return 0;
	}

	@Override
	public void clearDictionary() {

	}
}
