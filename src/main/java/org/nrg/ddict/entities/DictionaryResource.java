/*
 * org.nrg.ddict.entities.Resource
 *
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * XNAT is an open-source project of the Neuroinformatics Research Group.
 * Released under the Simplified BSD.
 *
 * Last modified 5/1/14 10:30 AM
 */

package org.nrg.ddict.entities;

import org.nrg.ddict.entities.impl.DictionaryItem;

import java.io.IOException;
import java.util.Map;

/**
 * Represents a resource within a data dictionary.
 */
public interface DictionaryResource {

	public void setId(String id);

	public String getId();

	public void addProperty(String id, DictionaryItem value);

	public void setProperties(Map<String,DictionaryItem> properties);

	public Map<String,DictionaryItem> getProperties();

	public String toJSON() throws IOException;

}
