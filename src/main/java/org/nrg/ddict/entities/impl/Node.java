/*
 * org.nrg.ddict.entities.Resource
 *
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * XNAT is an open-source project of the Neuroinformatics Research Group.
 * Released under the Simplified BSD.
 *
 * Last modified 5/1/14 10:30 AM
 */

package org.nrg.ddict.entities.impl;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a resource within a data dictionary.
 */
public class Node {

	private final String _uri;
	protected Map<String,Object> properties = new HashMap<String,Object>();

	public Node(String uri){
		this._uri = uri;
	}

	public String getURI(){
		return this._uri;
	}


	public void addProperty(String id, Object value){
		this.properties.put(id,value);
	};

	public Map<String,Object> getProperties(){
		return properties;
	}

	//public String toJSON() throws IOException;

}
