package org.nrg.ddict.entities.impl;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Created by James on 5/14/14.
 */
public abstract class DictionaryItem {

	@JsonIgnore
	public abstract boolean isLiteral();

	@JsonIgnore
	public abstract boolean isResource();
}
