package org.nrg.ddict.entities.impl;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Created by James on 5/14/14.
 */
public class DictionaryLiteral extends DictionaryItem{

	private String _value;

	public DictionaryLiteral(String value){
		this._value = value;
	}

	public String getValue(){ return this._value; }

	public void setValue(String value){ this._value = value; }

	public boolean isLiteral(){return true;}

	public boolean isResource(){return false;}
}
