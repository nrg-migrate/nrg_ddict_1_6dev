package org.nrg.ddict.entities.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.nrg.ddict.entities.DictionaryResource;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by James on 5/7/14.
 */
public class DefaultDictionaryResource extends DictionaryItem implements DictionaryResource {
	private String _id = "";
	private Map<String,DictionaryItem> _properties;

	public DefaultDictionaryResource(String id,Map<String,DictionaryItem> properties){
		this._properties = properties;
		this._id        = id;
	}

	public DefaultDictionaryResource(String id){
		this(id, new HashMap<String, DictionaryItem>());
	}

	public DefaultDictionaryResource(Map<String,DictionaryItem> properties){
		this(null,properties);
	}

	public void setId(String id){
		this._id = id;
	}

	public String getId() {
		return this._id;
	}

	public void addProperty(String id, DictionaryItem value){
		this._properties.put(id, value);
	}

	public void setProperties(Map<String,DictionaryItem> properties){
		this._properties = properties;
	}

	public Map<String,DictionaryItem> getProperties(){
		return this._properties;
	}

	public String toJSON() throws IOException {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		return ow.writeValueAsString(this);
	}

	public boolean isLiteral(){return false;}

	public boolean isResource(){return true;}
}
