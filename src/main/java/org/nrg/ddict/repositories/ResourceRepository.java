/*
 * org.nrg.ddict.repositories.ResourceDAO
 *
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * XNAT is an open-source project of the Neuroinformatics Research Group.
 * Released under the Simplified BSD.
 *
 * Last modified 5/1/14 10:32 AM
 */

package org.nrg.ddict.repositories;


import org.springframework.stereotype.Repository;

/**
 * Manages resource entities within the data dictionary framework.
 */
@Repository
public class ResourceRepository {
}
