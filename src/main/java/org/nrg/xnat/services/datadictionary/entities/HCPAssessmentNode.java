package org.nrg.xnat.services.datadictionary.entities;

/**
 * Created by James on 6/12/14.
 */
public class HCPAssessmentNode extends HCPNode {

	public HCPAssessmentNode(String uri){
		super(uri);
	}

	public void setCategory(HCPCategoryNode category){
		this.properties.put("hasCategory",category);
	}

	public HCPCategoryNode getCategory(){
		return (HCPCategoryNode)this.properties.get("category");
	}
}
