package org.nrg.xnat.services.datadictionary.entities;

import org.nrg.ddict.entities.impl.Node;

/**
 * Created by James on 6/12/14.
 */
public class HCPCategoryNode extends HCPNode {

	public HCPCategoryNode(String uri){
		super(uri);
	}
}
