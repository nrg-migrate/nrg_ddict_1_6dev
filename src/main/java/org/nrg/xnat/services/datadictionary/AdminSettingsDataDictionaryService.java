package org.nrg.xnat.services.datadictionary;

import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.vocabulary.XSD;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Iterator;
import java.util.Properties;

/**
 * Created by James on 6/26/14.
 */
@Service
public class AdminSettingsDataDictionaryService implements InitializingBean {
	public static String SERVICE_NAME = "DataDictionaryService";
	private static final Log _log = LogFactory.getLog(SERVICE_NAME);

	@Value("${tdb.admin-dataset.dir}")
	private String _datasetDir;

	private Dataset _dataset;

	// This is a count so we can create unique uris for each Jena Individual.
	// e.g. http://www.xnat.org/dataDictionary/adminSettings/tab_2
	private int _count = 0;

	@Inject
	@Named("dataDictionary")
	private Properties _properties;

	private final String NS = "http://www.xnat.org/dataDictionary/adminSettings/";

	public void afterPropertiesSet(){
		_log.debug("Initializing admin settings data dictionary service");
		createDataDictionary();
		initAdminConfiguration(_properties.getProperty("admin_ui"));
	}

	//TODO: For testing only.
	public void printModel(){
		_dataset.begin(ReadWrite.READ);
		this.getOntModel().write(System.out);
		_dataset.end();
	}

	// TODO: For testing only.
	public void clearDictionary(){
		_dataset.begin(ReadWrite.WRITE);
		_dataset.getDefaultModel().removeAll();
		_dataset.commit();
		_dataset.end();
	}

	/**
	 * Function will print all elements in the Admin Settings data dictionary.
	 */
	public void printElements(){
		_dataset.begin(ReadWrite.READ);
		OntModel om = this.getOntModel();
		try{
			// For each individual, print the properties of that individual.
			printProperties(om.getIndividual(NS + "configuration/adminSettings"),"");
		}finally {
			_dataset.end();
		}
	}

	/**
	 * Recursive function that prints all properties (name --> value) of an individual.
	 * @param i - The individual we are interested in.
	 * @param spacing - The amount to indent before printing the property. This is for readability.
	 */
	private void printProperties(Individual i, String spacing){
		if(i.isClass()){ return; } // Don't print classes

		// Print the label and URI of this Individual
		System.out.print(spacing + i.getLabel(null) + " (" + i.getURI() + ")\n");

		StmtIterator si = i.listProperties(); // Iterate over each property
		while(si.hasNext()){
			Statement s = si.next();
			// If the property is a resource, print the properties for that resource.
			if(s.getObject().isResource()){
				Individual ind = i.getOntModel().getIndividual(s.getResource().getURI());
				printProperties(ind, spacing + "\t");
			}else{
				// If the property is a literal, print the name --> value for that property.
				System.out.println(spacing + "\t" + s.getPredicate().getLocalName() + "--> " + s.getObject().toString());
			}
		}
	}

	/**
	 * Function creates the Admin Configuration Data Dictionary from json in the configuration file.
	 * @param items - String of JSON to be inserted into the Data Dictionary.
	 */
	private void initAdminConfiguration(String items){
		JSONObject jo;
		try{
			// Parse the JSON and create a new JSONObject
			jo = new JSONObject("{ items : " + items.replace("\n", "") + "}").getJSONArray("items").getJSONObject(0);
		}catch(Exception e){
			_log.error("Failed to convert JSON into a JSONObject. ", e);
			return;
		}

		_dataset.begin(ReadWrite.WRITE);
		OntModel om = this.getOntModel();

		// Create a parent individual for all admin configuration settings.
		Individual adminConfig = om.createIndividual(NS + "configuration/adminSettings",om.getOntClass(NS + "configuration"));
		adminConfig.addLabel(ResourceFactory.createPlainLiteral("Admin Configuration Settings"));

		addProperties(jo, adminConfig); // Add all properties to the admin configuration

		_dataset.commit();
		_dataset.end();
	}

	/**
	 * This function creates a new Individual with a unique URI.
	 * @param oClass - The ontClass to use to create the individual.
	 * @param uri - The base uri to use for this Individual.
	 * @param label - The label to give this individual.
	 * @return Individual - the newly created Individual.
 	 */
	private Individual createIndividual(OntClass oClass, String uri, String label){
		Individual i = oClass.createIndividual(uri + "_" + _count);
		i.addLabel(ResourceFactory.createPlainLiteral(label));
		_count++;
		return i;
	}

	/**
	 * Function will add all properties in the JSONObject jo to the Individual ind
	 * @param jo - The JSONObject containing the properties.
	 * @param ind - The Individual we wish to add the properties to.
	 */
	private void addProperties(JSONObject jo, Individual ind){
		OntModel om = ind.getOntModel();

		// Loop over all the keys in the JSONObject
		Iterator<String> keys = jo.keys();
		while(keys.hasNext()){
			String key = keys.next();
			try{
				// Test whether the value of this key is another JSONObject.
				JSONObject jsonObject = jo.optJSONObject(key);
				if(jsonObject != null){
					//Create a new property value and add it to the parent Individual (ind).
					Individual new_ind = createIndividual(om.getOntClass(NS + "configuration/" + key), NS + key, key);
					ind.setPropertyValue(om.getProperty(NS + "property/" + key), new_ind);
					addProperties(jsonObject, new_ind); // Add properties to the new Individual (new_ind)
					continue;
				}

				// Test whether the value of this key is a JSONArray.
				JSONArray jsonArray = jo.optJSONArray(key);
				if(jsonArray != null){
					addProperties(key, jsonArray, ind);
					continue;
				}

				// Finally, est whether the value of this key is a String.
				String value = jo.optString(key);
				if(value != null){
					// Add the property to the parent Individual.
					ind.addProperty(om.getOntProperty(NS + "property/" + key), value);
					continue;
				}

			} catch (Exception e) {
				_log.error("Failed to add property, " + key + ", to the data dictionary.");
				//e.printStackTrace();
			}
		}
	}

	/**
	 * This function adds Properties contained in a JSONArray.
	 * @param key - The key corresponding to the value of this JSON array. (e.g. "tabs":[{...},{...}]
	 *              This is used to create new individuals as we parse.
	 * @param jsonArray - The JSONArray containing the properties.
	 * @param parent - The parent Individual.
	 */
	private void addProperties(String key, JSONArray jsonArray, Individual parent){
		OntModel om = this.getOntModel();

		// Loop through the json array.
		for(int i = 0; i< jsonArray.length(); i++){

			// Test whether the value of this element is a JSONObject.
			JSONObject jsonObject = jsonArray.optJSONObject(i);
			if(jsonObject != null){
				//Create a new property value and add it to the parent Individual.
				Individual new_ind = createIndividual(om.getOntClass(NS + "configuration/" + key), NS + key, key);
				parent.addProperty(om.getProperty(NS + "property/" + key), new_ind);
				addProperties(jsonObject,new_ind); // Add properties to the new Individual (new_ind)
				continue;
			}

			// Test whether the value of this element is a String.
			String value = jsonArray.optString(i);
			if(value != null){
				// Add the property to the parent Individual.
				parent.addProperty(om.getOntProperty(NS + "property/" + key), value);
				continue;
			}
		}
	}


    /**
	 * Function defines the model of the data dictionary.
	 * TODO: I'm not sure if we actually need to define this stuff.
	 * TODO: Maybe we can just parse the JSON and create the classes / properties on the fly as we need them.
	 */
	private void createDataDictionary(){
		_dataset = TDBFactory.createDataset(_datasetDir);
		_dataset.begin(ReadWrite.WRITE);
		OntModel om = getOntModel();

		OntClass cAdminSettings = om.createClass(NS + "configuration");
		OntClass cTab           = om.createClass(NS + "configuration/tabs");
		OntClass cAttribute     = om.createClass(NS + "configuration/attributes");
		OntClass cAttributeX    = om.createClass(NS + "configuration/attributesx");
		OntClass cSection       = om.createClass(NS + "configuration/sections");
		OntClass cElement       = om.createClass(NS + "configuration/elements");
		OntClass cToolTip       = om.createClass(NS + "configuration/tooltip");
		OntClass cAccess        = om.createClass(NS + "configuration/access");
		OntClass cValidation    = om.createClass(NS + "configuration/validation");
		OntClass cOption        = om.createClass(NS + "configuration/options");
		OntClass cStyle         = om.createClass(NS + "configuration/style");

		// Admin Settings Properties //

		DatatypeProperty pUri = om.createDatatypeProperty(NS + "property/uri");
		pUri.addLabel(ResourceFactory.createPlainLiteral("uri"));
		pUri.setDomain(XSD.Name);
		pUri.addRange(cAdminSettings);
		pUri.addRange(cTab);

		DatatypeProperty pUriDevNotes = om.createDatatypeProperty(NS + "property/uri-dev-notes");
		pUriDevNotes.addLabel(ResourceFactory.createPlainLiteral("uri-dev-notes"));
		pUriDevNotes.setDomain(XSD.Name);
		pUriDevNotes.addRange(cAdminSettings);

		DatatypeProperty pMode = om.createDatatypeProperty(NS + "property/mode");
		pMode.addLabel(ResourceFactory.createPlainLiteral("mode"));
		pMode.setDomain(XSD.Name);
		pMode.addRange(cAdminSettings);

		DatatypeProperty pModeDevNotes = om.createDatatypeProperty(NS + "property/mode-dev-notes");
		pModeDevNotes.addLabel(ResourceFactory.createPlainLiteral("mode-dev-notes"));
		pModeDevNotes.setDomain(XSD.Name);
		pModeDevNotes.addRange(cAdminSettings);

		ObjectProperty pTab = om.createObjectProperty(NS + "property/tab");
		pTab.addLabel(ResourceFactory.createPlainLiteral("tab"));
		pTab.setDomain(cTab);
		pTab.addRange(cAdminSettings);

		DatatypeProperty pName = om.createDatatypeProperty(NS + "property/name");
		pName.addLabel(ResourceFactory.createPlainLiteral("name"));
		pName.setDomain(XSD.Name);
		pName.addRange(cTab);
		pName.addRange(cAttribute);
		pName.addRange(cElement);

		DatatypeProperty pNameDevNotes = om.createDatatypeProperty(NS + "property/name-dev-notes");
		pNameDevNotes.addLabel(ResourceFactory.createPlainLiteral("name-dev-notes"));
		pNameDevNotes.setDomain(XSD.Name);
		pNameDevNotes.addRange(cTab);

		DatatypeProperty pId = om.createDatatypeProperty(NS + "property/id");
		pId.addLabel(ResourceFactory.createPlainLiteral("id"));
		pId.setDomain(XSD.Name);
		pId.addRange(cTab);
		pId.addRange(cSection);
		pId.addRange(cElement);
		pId.addRange(cOption);

		DatatypeProperty pIdDevNotes = om.createDatatypeProperty(NS + "property/id-dev-notes");
		pIdDevNotes.addLabel(ResourceFactory.createPlainLiteral("id-dev-notes"));
		pIdDevNotes.setDomain(XSD.Name);
		pIdDevNotes.setRange(cTab);

		DatatypeProperty pLabel = om.createDatatypeProperty(NS + "property/label");
		pLabel.addLabel(ResourceFactory.createPlainLiteral("label"));
		pLabel.setDomain(XSD.Name);
		pLabel.addRange(cTab);
		pLabel.addRange(cSection);
		pLabel.addRange(cElement);
		pLabel.addRange(cOption);

		DatatypeProperty pActive = om.createDatatypeProperty(NS + "property/active");
		pActive.addLabel(ResourceFactory.createPlainLiteral("active"));
		pActive.setDomain(XSD.Name);
		pActive.addRange(cTab);

		DatatypeProperty pActiveDevNotes = om.createDatatypeProperty(NS + "property/active-dev-notes");
		pActiveDevNotes.addLabel(ResourceFactory.createPlainLiteral("active-dev-notes"));
		pActiveDevNotes.setDomain(XSD.Name);
		pActiveDevNotes.addRange(cTab);

		DatatypeProperty pClassNames = om.createDatatypeProperty(NS + "property/classNames");
		pClassNames.addLabel(ResourceFactory.createPlainLiteral("classNames"));
		pClassNames.setDomain(XSD.Name);
		pClassNames.addRange(cTab);
		pClassNames.addRange(cSection);
		pClassNames.addRange(cElement);
		pClassNames.addRange(cOption);

		DatatypeProperty pActiveComment = om.createDatatypeProperty(NS + "property/activeComment");
		pActiveComment.addLabel(ResourceFactory.createPlainLiteral("activeComment"));
		pActiveComment.setRange(XSD.Name);
		pActiveComment.addRange(cTab);

		DatatypeProperty pClassNamesDevNotes = om.createDatatypeProperty(NS + "property/classNames-dev-notes");
		pClassNamesDevNotes.addLabel(ResourceFactory.createPlainLiteral("classNames-dev-notes"));
		pClassNamesDevNotes.setDomain(XSD.Name);
		pClassNamesDevNotes.addRange(cTab);

		ObjectProperty pAttributeX = om.createObjectProperty(NS + "property/attributeX");
		pAttributeX.addLabel(ResourceFactory.createPlainLiteral("attributeX"));
		pAttributeX.setDomain(cAttributeX);
		pAttributeX.addRange(cTab);

		DatatypeProperty pTitle = om.createDatatypeProperty(NS + "property/title");
		pTitle.addLabel(ResourceFactory.createPlainLiteral("title"));
		pTitle.setDomain(XSD.Name);
		pTitle.addRange(cAttributeX);

		ObjectProperty pAttribute = om.createObjectProperty(NS + "property/attributes");
		pAttribute.addLabel(ResourceFactory.createPlainLiteral("attribute"));
		pAttribute.setDomain(cAttribute);
		pAttribute.addRange(cTab);
		pAttribute.addRange(cSection);
		pAttribute.addRange(cElement);

		DatatypeProperty pValue = om.createDatatypeProperty(NS + "property/value");
		pValue.addLabel(ResourceFactory.createPlainLiteral("value"));
		pValue.setDomain(XSD.Name);
		pValue.addRange(cAttribute);
		pValue.addRange(cElement);

		DatatypeProperty pHeader = om.createDatatypeProperty(NS + "property/header");
		pHeader.addLabel(ResourceFactory.createPlainLiteral("header"));
		pHeader.setDomain(XSD.Name);
		pHeader.addRange(cTab);

		DatatypeProperty pFooter = om.createDatatypeProperty(NS + "property/footer");
		pFooter.addLabel(ResourceFactory.createPlainLiteral("footer"));
		pFooter.setDomain(XSD.Name);
		pFooter.addRange(cTab);

		DatatypeProperty pSectionsDevNotes = om.createDatatypeProperty(NS + "property/sections-dev-notes");
		pSectionsDevNotes.addLabel(ResourceFactory.createPlainLiteral("sections-dev-notes"));
		pSectionsDevNotes.setDomain(XSD.Name);
		pSectionsDevNotes.addRange(cTab);

		ObjectProperty pSection = om.createObjectProperty(NS + "property/section");
		pSection.addLabel(ResourceFactory.createPlainLiteral("section"));
		pSection.setDomain(cSection);
		pSection.addRange(cTab);

		// Section Attributes //

		DatatypeProperty pDescription = om.createDatatypeProperty(NS + "property/description");
		pDescription.addLabel(ResourceFactory.createPlainLiteral("description"));
		pDescription.setDomain(XSD.Name);
		pDescription.addRange(cSection);
		pDescription.addRange(cElement);
		pDescription.addRange(cOption);

		ObjectProperty pElement = om.createObjectProperty(NS + "property/element");
		pElement.addLabel(ResourceFactory.createPlainLiteral("element"));
		pElement.setDomain(cElement);
		pElement.addRange(cSection);

		ObjectProperty pToolTip = om.createObjectProperty(NS + "property/tooltip");
		pToolTip.addLabel(ResourceFactory.createPlainLiteral("tooltip"));
		pToolTip.setDomain(cToolTip);
		pToolTip.addRange(cElement);

		DatatypeProperty pType = om.createDatatypeProperty(NS + "property/type");
		pType.addLabel(ResourceFactory.createPlainLiteral("type"));
		pType.setDomain(XSD.Name);
		pType.addRange(cToolTip);
		pType.addRange(cElement);
		pType.addRange(cValidation);

		DatatypeProperty pText = om.createDatatypeProperty(NS + "property/text");
		pText.addLabel(ResourceFactory.createPlainLiteral("text"));
		pText.setDomain(XSD.Name);
		pText.addRange(cToolTip);

		DatatypeProperty pRequired = om.createDatatypeProperty(NS + "property/required");
		pRequired.addLabel(ResourceFactory.createPlainLiteral("required"));
		pRequired.setDomain(XSD.Name);
		pRequired.addRange(cElement);

		DatatypeProperty pDisabled = om.createDatatypeProperty(NS + "property/disabled");
		pDisabled.addLabel(ResourceFactory.createPlainLiteral("disabled"));
		pDisabled.setDomain(XSD.Name);
		pDisabled.addRange(cElement);

		ObjectProperty pAccess = om.createObjectProperty(NS + "property/access");
		pAccess.addLabel(ResourceFactory.createPlainLiteral("access"));
		pAccess.setDomain(cAccess);
		pAccess.addRange(cElement);

		DatatypeProperty pAdmin = om.createDatatypeProperty(NS + "property/admin");
		pAdmin.addLabel(ResourceFactory.createPlainLiteral("admin"));
		pAdmin.setDomain(XSD.Name);
		pAdmin.addRange(cAccess);

		DatatypeProperty pAdvanced = om.createDatatypeProperty(NS + "property/advanced");
		pAdvanced.addLabel(ResourceFactory.createPlainLiteral("advanced"));
		pAdvanced.setDomain(XSD.Name);
		pAdvanced.addRange(cAccess);

		DatatypeProperty pBasic = om.createDatatypeProperty(NS + "property/basic");
		pBasic.addLabel(ResourceFactory.createPlainLiteral("basic"));
		pBasic.setDomain(XSD.Name);
		pBasic.addRange(cAccess);

		DatatypeProperty pUser = om.createDatatypeProperty(NS + "property/user");
		pUser.addLabel(ResourceFactory.createPlainLiteral("user"));
		pUser.setDomain(XSD.Name);
		pUser.addRange(cAccess);

		DatatypeProperty pDev = om.createDatatypeProperty(NS + "property/dev");
		pDev.addLabel(ResourceFactory.createPlainLiteral("dev"));
		pDev.setDomain(XSD.Name);
		pDev.addRange(cAccess);

		ObjectProperty pValidation = om.createObjectProperty(NS + "property/validation");
		pValidation.addLabel(ResourceFactory.createPlainLiteral("validation"));
		pValidation.setDomain(cValidation);
		pValidation.addRange(cElement);

		DatatypeProperty pOn = om.createDatatypeProperty(NS + "property/on");
		pOn.addLabel(ResourceFactory.createPlainLiteral("on"));
		pOn.setDomain(XSD.Name);
		pOn.addRange(cValidation);

		DatatypeProperty pMaxLength = om.createDatatypeProperty(NS + "property/maxlength");
		pMaxLength.addLabel(ResourceFactory.createPlainLiteral("maxlength"));
		pMaxLength.setDomain(XSD.Name);
		pMaxLength.addRange(cValidation);

		DatatypeProperty pMinLength = om.createDatatypeProperty(NS + "property/minlength");
		pMinLength.addLabel(ResourceFactory.createPlainLiteral("minlength"));
		pMinLength.setDomain(XSD.Name);
		pMinLength.addRange(cValidation);

		ObjectProperty pOptions = om.createObjectProperty(NS + "property/options");
		pOptions.addLabel(ResourceFactory.createPlainLiteral("options"));
		pOptions.setDomain(cOption);
		pOptions.addRange(cElement);

		DatatypeProperty pIsDefault = om.createDatatypeProperty(NS + "property/isDefault");
		pIsDefault.addLabel(ResourceFactory.createPlainLiteral("isDefault"));
		pIsDefault.setDomain(XSD.Name);
		pIsDefault.addRange(cOption);

		DatatypeProperty pSelected = om.createDatatypeProperty(NS + "property/selected");
		pSelected.addLabel(ResourceFactory.createPlainLiteral("selected"));
		pSelected.setDomain(XSD.Name);
		pSelected.addRange(cOption);

		DatatypeProperty pFunction = om.createDatatypeProperty(NS + "property/function");
		pFunction.addLabel(ResourceFactory.createPlainLiteral("function"));
		pFunction.setDomain(XSD.Name);
		pFunction.addRange(cElement);

		ObjectProperty pStyle = om.createObjectProperty(NS + "property/style");
		pStyle.addLabel(ResourceFactory.createPlainLiteral("style"));
		pStyle.setDomain(cStyle);
		pStyle.addRange(cElement);

		DatatypeProperty pMargin = om.createDatatypeProperty(NS + "property/margin");
		pMargin.addLabel(ResourceFactory.createPlainLiteral("margin"));
		pMargin.setDomain(XSD.Name);
		pMargin.addRange(cStyle);

		DatatypeProperty pWidth = om.createDatatypeProperty(NS + "property/width");
		pWidth.addLabel(ResourceFactory.createPlainLiteral("width"));
		pWidth.setDomain(XSD.Name);
		pWidth.addRange(cStyle);

		DatatypeProperty pColor = om.createDatatypeProperty(NS + "property/color");
		pColor.addLabel(ResourceFactory.createPlainLiteral("color"));
		pColor.setDomain(XSD.Name);
		pColor.addRange(cStyle);

		DatatypeProperty pDisabledDevNotes = om.createDatatypeProperty(NS + "property/disabled-dev-notes");
		pDisabledDevNotes.addLabel(ResourceFactory.createPlainLiteral("disabled-dev-notes"));
		pDisabledDevNotes.setDomain(XSD.Name);
		pDisabledDevNotes.addRange(cElement);

		_dataset.commit();
		_dataset.end();
	}

	/**
	 * Function returns the OWL Ontology model from the dataset.
	 * TODO: Move to DataDictionaryService class
	 * @return OntModel
	 */
	private OntModel getOntModel(OntModelSpec spec){
		return ModelFactory.createOntologyModel(spec, _dataset.getDefaultModel());
	}

	/**
	 * Function returns the OWL Ontology model from the dataset.
	 * TODO: Move to DataDictionaryService class
	 * @return OntModel
	 */
	private OntModel getOntModel(){
		return this.getOntModel(new OntModelSpec(OntModelSpec.OWL_MEM_TRANS_INF));
	}

}
