package org.nrg.xnat.services.datadictionary.entities;

import java.util.List;

/**
 * Created by James on 6/12/14.
 */
public class HCPAttributeNode extends HCPNode {

	public HCPAttributeNode(String uri){
		super(uri);
	}

	public void setCategory(HCPCategoryNode category){
		this.properties.put("hasCategory", category);
	}

	public void setAssessment(HCPAssessmentNode assessment){
		this.properties.put("hasAssessment",assessment);
	}

	public void setDictType(String dictType){
		this.properties.put("dictType", dictType);
	}

	public void setValidationMessage(String message){
		this.properties.put("validationMessage", message);
	}

	public void setValidation(String validation){
		this.properties.put("validation", validation);
	}

	public void setOperators(List<String> operators){
		this.properties.put("operators",operators);
	}

	public void setWatermark(String watermark){
		this.properties.put("watermark",watermark);
	}

	public void setXSIType(String xsiType){
		this.properties.put("xsiType",xsiType);
	}



	public void getCategory(){
		this.properties.get("hasCategory");
	}

	public void getAssessment(){
		this.properties.get("hasAssessment");
	}

	public void getDictType(){
		this.properties.get("dictType");
	}

	public void getValidationMessage(){
		this.properties.get("validationMessage");
	}

	public void getValidation(){
		this.properties.get("validation");
	}

	public void getOperators(){
		this.properties.get("operators");
	}

	public void getWatermark(){
		this.properties.get("watermark");
	}

	public void getXSIType(){
		this.properties.get("xsiType");
	}
}
