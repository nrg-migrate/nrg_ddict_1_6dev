package org.nrg.xnat.services.datadictionary;

import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.XSD;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.nrg.xnat.services.datadictionary.entities.HCPAssessmentNode;
import org.nrg.xnat.services.datadictionary.entities.HCPAttributeNode;
import org.nrg.xnat.services.datadictionary.entities.HCPCategoryNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Iterator;
import java.util.Properties;

/**
 * Created by James on 6/5/14.
 */
@Service
public class HCPDataDictionaryService implements InitializingBean {

	public static String SERVICE_NAME = "DataDictionaryService";
	private static final Log _log = LogFactory.getLog(SERVICE_NAME);

	@Inject
	@Named("dataDictionary")
	private Properties _properties;

    @Value("${tdb.hcp-dataset.dir}")
	private String _datasetDir;

	//TODO: this is broken.
	//@Inject
	//private DataDictionaryService _dataDictionary;

	private Dataset _dataset;

	private final String NS = "http://www.xnat.org/dataDictionary/hcp/";

	public HCPDataDictionaryService(){
	}

	public void afterPropertiesSet() throws Exception {
		_log.debug("Initializing simple data dictionary service from JSON property");
		createDictionary();
		initItems("category",   _properties.getProperty("categories"));
		initItems("assessment", _properties.getProperty("assessments"));
		initItems("attribute",  _properties.getProperty("attributes"));
	}

	// TODO: For testing only.
	public void clearDictionary(){
		_dataset.begin(ReadWrite.WRITE);
		_dataset.getDefaultModel().removeAll();
		_dataset.commit();
		_dataset.end();
	}

	/**
	 * Function will print all individuals of a class.
	 * TODO: Move to DataDictionaryService class
	 * @param uri - the URI of the class we are interested in.
	 */
	private void printAllIndividuals(String uri){
		_dataset.begin(ReadWrite.READ);
		OntModel om = this.getOntModel();
		try{
			// Get an iterator for all instances of this class.
			ExtendedIterator instances = om.getOntClass(uri).listInstances();
			while(instances.hasNext()){

				// Print the name and URI of the individual.
				Individual i = (Individual)instances.next();
				System.out.println(i.getPropertyValue(om.getProperty(NS + "name")));
				System.out.println("\turi--> "+i.getURI());

				// Print the name and value for each property belonging to this individual.
				StmtIterator si = i.listProperties();
				while(si.hasNext()){
					Statement s = si.next();
					String value = "";
					if(s.getObject().isResource()){
						// If the object is a resource print the name of the resource instead of the URI (readability)
						Statement p = s.getObject().asResource().getProperty(om.getOntProperty(NS + "name"));
						if(p != null){ value = p.getObject().toString(); }
					}
					if(value.isEmpty() || value.equals("")){
						// If the value is still empty, set it to the string of the object.
						// If the object is a resource then this will be the URI, otherwise it will be the string literal.
						value = s.getObject().toString();
					}

					System.out.println("\t" + s.getPredicate().getLocalName() + "--> " + value);
				}
			}
		}finally {
			_dataset.end();
		}
	}

	/**
	 * Function will create and insert all items into the data dictionary.
	 * @param itemType - the type of item to insert.
	 * @param items    - json string containing items
	 */
	private void initItems(String itemType, String items){

		JSONArray jarr;
		try{
			// Parse the categories JSONArray.
			jarr = new JSONObject("{ items : " + items.replace("\n", "") + "}").getJSONArray("items");
		}catch(Exception e){
			_log.error("Failed to convert " + itemType + " JSON into a JSONOBject. ", e);
			return;
		}

		_dataset.begin(ReadWrite.WRITE);
		OntModel om = this.getOntModel();
		OntClass item = om.getOntClass(NS + itemType);

		// Insert each category into the data dictionary
		for(int i = 0; i<jarr.length(); i++){
			try{
				JSONObject jo = jarr.getJSONObject(i);
				// Create an entry for the category and set all properties.
				setProperties(item.createIndividual(NS + itemType + "/" + URIUtil.encodeQuery(jo.getString("name"))),jo);
			}catch(Exception e){
				_log.error("Failed to insert " + itemType +" into the triplestore. ", e);
			}
		}
		_dataset.commit();
		_dataset.end();
	}

	/**
	 * Function loops through each property in the json object, creates Jena properties for each and adds each
	 * property to the resource r.
	 * @param r - the resource we wish to add properties to.
	 * @param properties - properties we wish to add.
	 */
	private void setProperties(OntResource r, JSONObject properties){
		OntModel om = r.getOntModel();
		Iterator<String> it = properties.keys();
		while(it.hasNext()){
			String key = it.next();
			try{
				if(key.equals("category") || key.equals("assessment")){
					// Categories and assessment are object Properties, so they are a special case. 
					// call encodeQuery on each entry because some have spaces / special characters.
					// Each Category and Assessment Individual should have already been created.
					r.setPropertyValue(om.getProperty(NS + key), om.getIndividual(NS + key + "/" + URIUtil.encodeQuery(properties.getString(key))));
				}else if(key.equals("projects")){
					// Call function to add project properties.
					// This function will create the project individual if it doesn't exist.
					addProjectProperties(properties.getString("projects").replaceAll("\"|\"|\\[|\\]", "").split(","), r);
				}else if(key.equals("operators")){
					// Call function to add operator properties.
					// This function will create the operator individual if it doesn't exist.
					addOperatorProperties(properties.getJSONObject("operators"), r);
				}else if(key.equals("values")){
					r.setPropertyValue(om.getProperty(NS + "values"), ResourceFactory.createPlainLiteral(properties.getJSONObject("values").toString()));
				}else{
					// All other properties are just string literals.
					r.setPropertyValue(om.getProperty(NS + key), ResourceFactory.createPlainLiteral(properties.getString(key)));
				}
			}catch(Exception e){
				_log.error("Failed to set property value for '" + key + "'",e);
			}
		}
	}

	/**
	 * Function creates an individual for each property in the operators array if one doesn't already
	 * exist in the mode. Then attaches the individual to the resource's belongsToProject property.
	 * @param projects - array of property values to add
	 * @param r - the Resource we are adding properties to
	 */
	private void addProjectProperties(String[] projects, OntResource r){
		OntModel om = r.getOntModel();
		for(String project : projects){
			// Get the individual for this project
			Individual pi = om.getIndividual(NS + "project/" + project);
			if(pi == null){
				// If the individual doesn't exist, create one.
				pi = om.getOntClass(NS + "project").createIndividual(NS + "project/" + project);
				pi.setPropertyValue(om.getProperty(NS + "name"), ResourceFactory.createPlainLiteral(project));
			}
			// Set the belongsToProject property for the resource.
			r.addProperty(om.getProperty(NS + "belongsToProject"), pi);
		}
	}

	/**
	 * Function creates an individual for each operator in the operators array if one doesn't already
	 * exist in the mode. Then attaches the individual to the resource's hasOperator property.
	 * @param operators - JSON Object containing operator properties to add.
	 * @param r - the resource we are adding properties to.
	 * @throws Exception
	 */
	private void addOperatorProperties(JSONObject operators, OntResource r) throws Exception{
		OntModel om = r.getOntModel();
		Iterator<String> keys = operators.keys();
		while(keys.hasNext()){
			String key = keys.next();
			String o = operators.getString(key);
			key = URIUtil.encodeQuery(key);

			Individual oi = om.getIndividual(NS + "operator/" + key);
			if(oi == null){
				// If the individual doesn't exist, create one.
				oi = om.getOntClass(NS + "operator").createIndividual(NS + "operator/" + key);
				oi.setPropertyValue(om.getProperty(NS + "name"), ResourceFactory.createPlainLiteral(o));
			}
			// Set the hasOperator property for the resource.
			r.addProperty(om.getProperty(NS + "hasOperator"), oi);
		}
	}

	/**
	 * Function defines all OWL classes and properties.
	 */
	private void createDictionary(){
		_dataset = TDBFactory.createDataset(_datasetDir);
		_dataset.begin(ReadWrite.WRITE);
		OntModel om = getOntModel();

		// Define the Category, Assessment and Attribute classes
		OntClass category   = om.createClass(NS + "category");
		OntClass assessment = om.createClass(NS + "assessment");
		OntClass attribute  = om.createClass(NS + "attribute");
		OntClass project    = om.createClass(NS + "project");
		OntClass operator   = om.createClass(NS + "operator");

		// Define the name property.
		DatatypeProperty name = om.createDatatypeProperty(NS + "name");
		name.addLabel("Name", "en");
		name.addRange(XSD.Name);
		name.addDomain(category);
		name.addDomain(assessment);
		name.addDomain(attribute);
		name.addDomain(project);

		// Define the column Header property.
		DatatypeProperty columnHeader = om.createDatatypeProperty(NS + "columnHeader");
		columnHeader.addLabel("Column Header", "en");
		columnHeader.addRange(XSD.Name);
		columnHeader.addDomain(category);
		columnHeader.addDomain(assessment);
		columnHeader.addDomain(attribute);

		// Define the Description property.
		DatatypeProperty description = om.createDatatypeProperty(NS + "description");
		description.addLabel("Description", "en");
		description.addRange(XSD.Name);
		description.addDomain(category);
		description.addDomain(assessment);
		description.addDomain(attribute);

		// Define the Tier property.
		DatatypeProperty tier = om.createDatatypeProperty(NS + "tier");
		tier.addLabel("Tier", "en");
		tier.addRange(XSD.Name);
		tier.addDomain(category);
		tier.addDomain(assessment);
		tier.addDomain(attribute);

		//Define the projects property.
		ObjectProperty projects = om.createObjectProperty(NS + "belongsToProject");
		projects.addLabel("Belongs To Project", "en");
		projects.addRange(project);
		projects.addDomain(category);
		projects.addDomain(assessment);
		projects.addDomain(attribute);

		//Define the Full Display Name property.
		DatatypeProperty fullDisplayName = om.createDatatypeProperty(NS + "fullDisplayName");
		fullDisplayName.addLabel("Full Display Name", "en");
		fullDisplayName.addRange(XSD.Name);
		fullDisplayName.addDomain(attribute);

		// Define the Dictionary Type property.
		DatatypeProperty dictType = om.createDatatypeProperty(NS + "dictType");
		dictType.addLabel("Dictionary Type","en");
		dictType.addRange(XSD.Name);
		dictType.addDomain(attribute);

		// Define the Validation Message property.
		DatatypeProperty validationMessage = om.createDatatypeProperty(NS + "validationMessage");
		validationMessage.addLabel("Validation Message","en");
		validationMessage.addRange(XSD.Name);
		validationMessage.addDomain(attribute);

		// Define the Validation property.
		DatatypeProperty validation = om.createDatatypeProperty(NS + "validation");
		validation.addLabel("Validation","en");
		validation.addRange(XSD.Name);
		validation.addDomain(attribute);

		// Define the Operators property.
		DatatypeProperty operators = om.createDatatypeProperty(NS + "hasOperator");
		operators.addLabel("Operators","en");
		operators.addRange(operator);
		operators.addDomain(attribute);

		// Define the Watermark Property.
		DatatypeProperty watermark = om.createDatatypeProperty(NS + "watermark");
		watermark.addLabel("Watermark","en");
		watermark.addRange(XSD.Name);
		watermark.addDomain(attribute);

		// Define the XSI Type Property.
		DatatypeProperty xsiType = om.createDatatypeProperty(NS + "xsiType");
		xsiType.addLabel("XSI Type","en");
		xsiType.addRange(XSD.Name);
		xsiType.addDomain(attribute);

		// Define the Field Id Property.
		DatatypeProperty fieldId = om.createDatatypeProperty(NS + "fieldId");
		fieldId.addLabel("Field Id","en");
		fieldId.addRange(XSD.Name);
		fieldId.addDomain(attribute);

		// Define the Values Property.
		DatatypeProperty values = om.createDatatypeProperty(NS + "values");
		values.addLabel("Values","en");
		values.addRange(XSD.Name);
		values.addDomain(attribute);

		// Define the Has Category property.
		ObjectProperty hasCategory = om.createObjectProperty(NS + "category");
		hasCategory.addLabel("Has Category","en");
		hasCategory.addRange(category);
		hasCategory.addDomain(attribute);

		// Define the Has Assessment property.
		ObjectProperty hasAssessment = om.createObjectProperty(NS + "assessment");
		hasAssessment.addLabel("Has Assessment","en");
		hasAssessment.addRange(assessment);
		hasAssessment.addDomain(attribute);

		_dataset.commit();
		_dataset.end();
	}

	/**
	 * Function prints all categories in the data dictionary.
	 */
	public void printCategories(){
		printAllIndividuals(NS + "category");
	}

	/**
	 * Function prints all assessments in the data dictionary.
	 */
	public void printAssessments(){
		printAllIndividuals(NS + "assessment");
	}

	/**
	 * Function prints all attributes in the data dictionary.
	 */
	public void printAttributes(){
		printAllIndividuals(NS + "attribute");
	}

	/**
	 * Function returns the OWL Ontology model from the dataset.
	 * TODO: Move to DataDictionaryService class
	 * @return OntModel
	 */
	private OntModel getOntModel(OntModelSpec spec){
		return ModelFactory.createOntologyModel(spec, _dataset.getDefaultModel());
	}

	/**
	 * Function returns the OWL Ontology model from the dataset.
	 * TODO: Move to DataDictionaryService class
	 * @return OntModel
	 */
	private OntModel getOntModel(){
		return this.getOntModel(new OntModelSpec(OntModelSpec.OWL_MEM_TRANS_INF));
	}

	/**
	 * Function inserts a category into the data dictionary.
	 * @param category - the category to insert.
	 */
	public void insertCategory(HCPCategoryNode category){
		//this._dataDictionary.insertNode(category);
	}

	/**
	 * Function inserts a assessments into the data dictionary.
	 * @param assessment - the assessment to insert.
	 */
	public void insertAssessment(HCPAssessmentNode assessment){
		//this._dataDictionary.insertNode(assessment);
	}

	/**
	 * Function inserts a attributes into the data dictionary.
	 * @param attribute - the attribute to insert.
	 */
	public void insertAttribute(HCPAttributeNode attribute){
		//this._dataDictionary.insertNode(attribute);
	}
}
