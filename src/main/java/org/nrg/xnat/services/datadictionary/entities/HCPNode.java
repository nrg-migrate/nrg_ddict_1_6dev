package org.nrg.xnat.services.datadictionary.entities;

import org.nrg.ddict.entities.impl.Node;

import java.util.List;

/**
 * Created by James on 6/12/14.
 */
public class HCPNode extends Node{

	public HCPNode(String uri){
		super(uri);
	}

	public void setName(String name){
		this.properties.put("name",name);
	}

	public void setColumnHeader(String columnHeader){
		this.properties.put("columnHeader",columnHeader);
	}

	public void setDescription(String description){
		this.properties.put("description",description);
	}

	public void setTier(String tier){
		this.properties.put("tier",tier);
	}

	public void setProjects(List<String> projects){
		this.properties.put("projects", projects);
	}


	public String getName(){
		return (String)this.properties.get("name");
	}

	public String getColumnHeader(){
		return (String)this.properties.get("columnHeader");
	}
}
