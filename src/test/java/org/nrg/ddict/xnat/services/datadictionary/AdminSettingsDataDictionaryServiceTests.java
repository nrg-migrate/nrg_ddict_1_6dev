package org.nrg.ddict.xnat.services.datadictionary;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.xnat.services.datadictionary.AdminSettingsDataDictionaryService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

/**
 * Created by James on 6/27/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AdminSettingsDataDictionaryServiceTests {

	@Inject
	private AdminSettingsDataDictionaryService _dictionary;

    // TODO: Restore this to clean up afterwards. For now it clears the dictionary after every test, so it fails after the first test.
//	@After
	public void clearDictionary(){
		_dictionary.clearDictionary();
	}

	@Test
	public void printModel(){
		_dictionary.printModel();
	}

	@Test
	public void printElements(){
		_dictionary.printElements();
	}
}
