package org.nrg.ddict.xnat.services.datadictionary;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.xnat.services.datadictionary.HCPDataDictionaryService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

/**
 * Created by James on 6/9/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class HCPDataDictionaryServiceTests {

	@Inject
	private HCPDataDictionaryService hcp_dictionary;

	@After
	public void clearDictionary(){
		hcp_dictionary.clearDictionary();
	}

	@Test
	public void printCategories(){

		System.out.println("\n== Categories ==");
		hcp_dictionary.printCategories();

		System.out.println("\n== Assessments ==");
		hcp_dictionary.printAssessments();

		System.out.println("\n== Attributes ==");
		hcp_dictionary.printAttributes();
	}
}
