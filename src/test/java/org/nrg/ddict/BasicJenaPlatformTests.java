package org.nrg.ddict;

import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.vocabulary.VCARD;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class BasicJenaPlatformTests {

    @Value("${tdb.dataset.dir}")
    private String _dataSetLocation;

	private Dataset _dataset;

	public BasicJenaPlatformTests() {
		_log.info("Creating test class");
	}

	@Before
	public void initialize() {
		_log.info("Initializing test class");
        final File location = new File(_dataSetLocation);
        if (!location.exists()) {
            boolean success = location.mkdirs();
            if (!success) {
                throw new RuntimeException("Failed to create the data set location folder: " + _dataSetLocation);
            }
        }
        _dataset = TDBFactory.createDataset(_dataSetLocation);
	}

	@After
	public void teardown() {
		_log.info("Tearing down test class");
		_dataset.begin(ReadWrite.WRITE);
		// Delete the model so the dataset is clean next time.
		DatasetAccessorFactory.create(_dataset).deleteDefault();
		_dataset.commit();
		_dataset.end();
	}

	@Test
	public void testBasicJenaFunctionality(){

		// Get the default model and put some data in it
		_dataset.begin(ReadWrite.WRITE);
		Model m = _dataset.getDefaultModel();

		m.createResource("http://somewhere/Becky_Smith")
				.addProperty(VCARD.FN, "Becky Smith")
				.addProperty(VCARD.N, m.createResource()
						.addProperty(VCARD.Given, "Becky")
						.addProperty(VCARD.Family, "Smith"));

		m.createResource("http://somewhere/John_Smith")
				.addProperty(VCARD.FN, "John Smith")
				.addProperty(VCARD.N, m.createResource()
						.addProperty(VCARD.Given, "John")
						.addProperty(VCARD.Family, "Smith"));

		m.createResource("http://somewhere/Sarah_Jones")
				.addProperty(VCARD.FN, "Sarah Jones")
				.addProperty(VCARD.N, m.createResource()
						.addProperty(VCARD.Given, "Sarah")
						.addProperty(VCARD.Family, "Jones"));

		_dataset.commit();
		_dataset.end();


		// Load the model from the dataset
		_dataset.begin(ReadWrite.READ);
		m = _dataset.getDefaultModel();
		_dataset.end();

		// Confirm there are two resources with the last name "Smith"
		Query q = QueryFactory.create("SELECT (COUNT(?g) as ?smith_count) " +
									  "WHERE { ?y <http://www.w3.org/2001/vcard-rdf/3.0#Family> \"Smith\" ." +
									  "        ?y <http://www.w3.org/2001/vcard-rdf/3.0#Given> ?g }");
		QueryExecution qe = QueryExecutionFactory.create(q,m);
		assertEquals(qe.execSelect().next().getLiteral("smith_count").getInt(), 2);

		// Confirm there is one resource with the last name "Jones"
		Query q2 = QueryFactory.create("SELECT (COUNT(?g) as ?jones_count) " +
									   "WHERE { ?y <http://www.w3.org/2001/vcard-rdf/3.0#Family> \"Jones\" ." +
									   "        ?y <http://www.w3.org/2001/vcard-rdf/3.0#Given> ?g }");
		qe = QueryExecutionFactory.create(q2,m);
		assertEquals(qe.execSelect().next().getLiteral("jones_count").getInt(), 1);


	}

	private static final Logger _log = LoggerFactory.getLogger(BasicJenaPlatformTests.class);
}
