/*
 * org.nrg.ddict.services.TestDataDictionaryServices
 *
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * XNAT is an open-source project of the Neuroinformatics Research Group.
 * Released under the Simplified BSD.
 *
 * Last modified 5/1/14 10:44 AM
 */

package org.nrg.ddict.services;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.ddict.entities.DictionaryResource;
import org.nrg.ddict.entities.impl.DefaultDictionaryResource;
import org.nrg.ddict.entities.impl.DictionaryItem;
import org.nrg.ddict.entities.impl.DictionaryLiteral;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertEquals;
/**
 * Tests the constituent and aggregate NRG data dictionary services.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class DataDictionaryServicesTests {
    public DataDictionaryServicesTests() {
        _log.info("Creating test class");
    }

    @Before
    public void initialize() {
        _log.info("Initializing test class");
    }

    @After
    public void teardown() {
        _log.info("Tearing down test class");
		ddict.clearDictionary();
    }

	@Test
	/**
	 * Tests adding and then removing a resource.
	 */
	public void testAddRemoveResource() throws Exception{
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

		// Add one Resource to the Data Dictionary
		Map<String,DictionaryItem> properties = new HashMap<String,DictionaryItem>();
		properties.put("TEST_PROPERTY", new DictionaryLiteral("SOME_VALUE"));
		properties.put("ANOTHER_TEST", new DictionaryLiteral("ANOTHER_VALUE"));
		DictionaryResource dr1 = new DefaultDictionaryResource("HELLO_WORLD",properties);
		ddict.addResource(dr1);

		// Confirm that the dictionary contains one resource
		assertEquals(1,ddict.getNumberOfResources());

		// Remove the resource
		ddict.removeResource(dr1);

		// Confirm that the dictionary is empty
		assertEquals(0,ddict.getNumberOfResources());

	}

    @Test
	/**
	 * Tests getting a resource as JSON
	 */
    public void testResourceToJson() throws Exception{

		// Add a Resource to the Data Dictionary
		Map<String,DictionaryItem> properties = new HashMap<String,DictionaryItem>();
		properties.put("TEST_PROPERTY", new DictionaryLiteral("SOME_VALUE"));
		properties.put("ANOTHER_TEST", new DictionaryLiteral("ANOTHER_VALUE"));

		Map<String,DictionaryItem> prop2 = new HashMap<String,DictionaryItem>();
		prop2.put("NESTED_RESOURCE", new DictionaryLiteral("WOW"));
		
		properties.put("RESOURCE_TEST", new DefaultDictionaryResource("RESOURCE_TEST",prop2));
		DictionaryResource dr1 = new DefaultDictionaryResource("HELLO_WORLD",properties);
		ddict.addResource(dr1);

		try {
			String json = dr1.toJSON();
			assertEquals(json, ddict.getResource("HELLO_WORLD").toJSON());
			System.out.println(json);
		} finally {
			ddict.removeResource(dr1);
		}
    }

    private static final Logger _log = LoggerFactory.getLogger(DataDictionaryServicesTests.class);

    @Autowired
    private DataDictionaryService ddict;
}
